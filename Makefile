ifndef prefix
    prefix = /usr/local
endif

all: install

install:
	@echo "Installing Pardus Xfce icon themes"
	mkdir -p $(DESTDIR)$(prefix)/share/icons
	@cp -fr pardus-xfce-icon-theme-metal $(DESTDIR)$(prefix)/share/icons

uninstall:
	@echo "Removing Pardus Xfce icon themes"
	@rm -fr $(DESTDIR)$(prefix)/share/icons/pardus-xfce-icon-theme-metal

.PHONY: install uninstall
